//Define Twilio Login Details
var twilioSID = 'ACf1ee42e8ae352c96a2e9df803a067941';
var twilioToken = '8d47bb40f0c42309da33d8dbf5551f56';

//Require Libraries
var Pusher = require("pusher");
var PeerIndexClient = require('./node_modules/peerindex-api/lib/peerindex.js');
var http = require('http');
var fs = require('fs');
var midi = require('midi');
var twilioClient = require('twilio')(twilioSID,twilioToken);
var sys = require('sys');
var url = require('url');
var sportsdata_nba = require('sportsdata').NBA;
sportsdata_nba.init('t', 2, 'tmcmpehaky7v3w5qb3mju8jv', '2012', 'REG');

//Define Pusher Login Details
var key = '1ccd6fd9880863b97f0d';
var secret = 'dd9e528ed20dca277c00';
var app_id = '38442';

//Instantiate API Objects
var pusher = new Pusher({appId:app_id,key:key,secret:secret});
var peerindex = new PeerIndexClient('43nb7asugf4ht3c3qgnkj9y3');
// var midiInput = new midi.input();

//---SET MODE---
var mode = process.argv[0];

//---FUNCTION FOR PEERINDEX MODE---
//RECENTLY RENDERED NON-FUNCTIONAL BECAUSE OF CHANGES
//TO THE RATE AT WHICH CALLS CAN BE MADE TO THE API
function peerindexMode(){
    var query = {twitter_screen_name: "barackobama"};
    peerindex.actorGraph(query,function(actor, result){
        var counter = 0;
        for(var i = 0; i < actor.influences.length; i++){
            var staggerAmount = Math.floor(i/5)*1500;
            setTimeout(function(){
                var query2 = actor['influences'][counter];
                counter++;
                peerindex.actorExtended(query2, function(actor2,result){
                    if(actor2){
                        makeNote(actor2.peerindex,actor2.authority,1000);
                        printToClient("PeerIndex says: "+actor2.peerindex);
                    }
                })
            },staggerAmount)
        }
    })
}

//---RECIEVE SMS FROM TWILIO---
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  var parsedUrl = url.parse("http://codeoclock.cloudapp.net"+req.url,true);
  if(parsedUrl.query.Body !== undefined){
    twilioFunction(parsedUrl.query.Body);
  }
  res.end();
}).listen(8080);

//---FUNCTION FOR TWILIO MODE---
var twilioFunction = function twilio(string){
    printToClient("Text recieved: " + string);
    if(!isNaN(parseInt(string))){
        makeNote(parseInt(string)%127, 1000);
    }
    else{
        makeNote(50 + string.length % 60, 1000);
    }
}

// //---FUNCTION FOR MIDI INPUT MODE---
// var notesOn = new Array();
// midiInput.on('message', function(deltaTime, message) {
//     var isInArray = false;

//     for(var i = 0; i<notesOn.length;i++){
//         if(message[1] == notesOn[i]){
//             isInArray = true;
//         }
//     }
//     if(!isInArray){
//         makeNote(message[1],1000);
//         printToClient("Midi Keyboard: "+message[1])
//         notesOn.push(message[1]);
//     }
//     if(message[2] == 127){
//         for(var i = 0; i<notesOn.length;i++){
//             if(message[1] == notesOn[i]){
//                 notesOn.splice(i,1);
//             }
//         }
//     }
// });

// ---MAKENOTE FUNCTION---
function makeNote(pitch, time){
    var d = new Date();
    var data = {};
    data.pitch = (pitch);
    data.velocity = 100;
    data.time = time;
    data.timestamp = d.getTime();
    pusher.trigger("sonifyEverything","note",data);
}

// ---PRINT TO CLIENT---
function printToClient(message){
    pusher.trigger("sonifyEverything","message",message);
    console.log(message);
}

//---NBA MODE---
function nba(){
    printToClient("We're playin' bas-ket-baaaalll!");
    sportsdata_nba.getSeasonSchedule(function(error, schedule) {
            if (!error) {
                var object = new Object();
                var teams = schedule.OptaFeed.OptaDocument[0].Team;
                for(var i = 0;i<schedule.OptaFeed.OptaDocument[0].MatchData.length;i++){
                    object[i] = schedule.OptaFeed.OptaDocument[0].MatchData[i].TeamData;
                }
                var n = 0;
                setInterval(function(){
                    var team1;
                    var team2;
                    for(var x = 0; x < teams.length; x++){
                        if(object[n][0].$.team_id == teams[x].$.uID){
                            team1 = teams[x].Name;
                        }
                        if(object[n][1].$.team_id == teams[x].$.uID){
                            team2 = teams[x].Name;
                        }
                    }
                    printToClient("NBA: "+team1+" beat "+team2);
                    makeNote(object[n][0].$.score - 24,1000);
                    makeNote(object[n][1].$.score - 24,1000);
                    n++;
                },1000)
            }
        });
}

//---OPEN SOUND---
makeNote(60,1000);
makeNote(64,1000);
makeNote(67,1000);

// ---SWITCH BASED ON MODE---
switch(mode){
    case "peerindex":
        peerindexMode();
        break;
    case "nba":
        nba();
        break;
}