var delay;
var pusher = new Pusher('1ccd6fd9880863b97f0d');
var channel = pusher.subscribe('sonifyEverything');

channel.bind('note', function(data) {
    var d = new Date();
    var latency = (d.getTime()-data.timestamp).valueOf();
    var t = 1000 - latency;
    if(t>0&&t<1000){
        setTimeout(function() {
            MIDI.noteOn(0, data.pitch, data.velocity, delay);
            MIDI.noteOff(0, data.pitch, delay + data.time/1000);
        }, t);
    }
    else{
        MIDI.noteOn(0, data.pitch, data.velocity, delay);
        MIDI.noteOff(0, data.pitch, delay + data.time/1000);
    }
});

channel.bind('message', function(data) {
        $("#outputPre").prepend(data+"\n");
});

window.onload = function () {
    MIDI.loadPlugin({
        soundfontUrl: "./soundfont/",
        instrument: "acoustic_grand_piano",
        callback: function() {
            delay = 0; // play one note every quarter second
            MIDI.setVolume(0, 127);
        }
    });
};